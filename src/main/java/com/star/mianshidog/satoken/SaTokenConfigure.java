package com.star.mianshidog.satoken;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * packageName com.star.mianshidog.satoken
 *
 * @author zerostart
 * @className SaTokenConfigure
 * @date 2024/10/10
 * @description
 */

@Configuration
public class SaTokenConfigure implements WebMvcConfigurer {
    // 注册 Sa-Token 拦截器

    /**
     * 注册 Sa-Token 拦截器(注解实现鉴权功能)
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**");
    }

    /**
     * SaToken 编写代码实现鉴权
     * @param registry
     */

    // @Deprecated 注解标记该方法已废弃，不建议使用
    @Deprecated
    public void addInterceptorsCode(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handle -> {
            StpUtil.isLogin();
        })).addPathPatterns("/**")
                .excludePathPatterns("/user/login");
    }
}
