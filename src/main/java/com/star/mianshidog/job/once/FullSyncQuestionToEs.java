package com.star.mianshidog.job.once;

import com.star.mianshidog.esdao.QuestionEsDao;
import com.star.mianshidog.model.dto.question.es.QuestionEsDTO;
import com.star.mianshidog.model.entity.Question;
import com.star.mianshidog.service.QuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * packageName com.star.mianshidog.job.once
 *
 * @author zerostart
 * @className FullSyncQuestionToEs
 * @date 2024/9/26
 * @description
 */
//@Component
@Slf4j
public class FullSyncQuestionToEs implements CommandLineRunner {

    @Resource
    private QuestionService questionService;

    @Resource
    private QuestionEsDao questionEsDao;

    @Override
    public void run(String... args) throws Exception {
        // 全量查出question数据
        List<Question> questionList = questionService.list();

        List<QuestionEsDTO> questionEsDTOList = questionList.stream().map(QuestionEsDTO::objToDto).collect(Collectors.toList());
        final int pageSize = 500;
        int total = questionEsDTOList.size();
        log.info("FullSyncQuestionToEs start, total {}", total);
        for (int i = 0; i < total; i += pageSize) {
            int end = Math.min(i + pageSize, total);
            log.info("sync from {} to {}", i, end);
            questionEsDao.saveAll(questionEsDTOList.subList(i, end));
        }
        log.info("FullSyncPostToEs end, total {}", total);
    }
}
