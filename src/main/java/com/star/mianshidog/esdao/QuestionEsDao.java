package com.star.mianshidog.esdao;

import com.star.mianshidog.model.dto.question.es.QuestionEsDTO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * packageName com.star.mianshidog.esdao
 *
 * @author zerostart
 * @className QuestionEsDao
 * @date 2024/9/26
 * @description
 */
public interface QuestionEsDao extends ElasticsearchRepository<QuestionEsDTO, Long> {

}
