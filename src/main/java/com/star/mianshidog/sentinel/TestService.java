package com.star.mianshidog.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import jodd.exception.ExceptionUtil;

/**
 * packageName com.star.mianshidog.sentinel
 *
 * @author zerostart
 * @className TestService
 * @date 2024/10/4
 * @description
 */
public class TestService {

    // 对应的handleException 函数位于`ExceptionUtil`中，并且必须为static函数
    @SentinelResource(value= "test", blockHandler = "handleException", blockHandlerClass = {ExceptionUtil.class})
    public void test() {
        System.out.println("test");
    }

    // 原函数
    @SentinelResource(value = "hello", blockHandler = "exceptionHandler", fallback = "helloFallback")
    public String hello(long s) {
        return "hello" + s;
    }

    // Fallback 函数，函数签名与原函数一致或加一个 Throwable 类型的参数.
    public String helloFallback(long s) {
        return "fallback" + s;
    }

    // Block 异常处理函数，参数最后多一个 BlockException，其余与原函数一致.
    public String exceptionHandler(long s, BlockException ex) {
        ex.printStackTrace();
        return " Oops, exceptionHandler" + s;
    }


}
