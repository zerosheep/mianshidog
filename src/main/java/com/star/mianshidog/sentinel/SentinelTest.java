package com.star.mianshidog.sentinel;

import com.alibaba.csp.sentinel.Entry;
import com.alibaba.csp.sentinel.SphU;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;

import java.util.ArrayList;
import java.util.List;

/**
 * packageName com.star.mianshidog.sentinel
 *
 * @author zerostart
 * @className SentinelTest
 * @date 2024/10/4
 * @description
 */
public class SentinelTest {

    // 自定义资源
    public static void main(String[] args) {
        Entry entry = null;
        // 务必保证finally被执行
        try {
            //
            entry = SphU.entry("test");
        } catch (BlockException e) {
            // 资源访问阻止，被降级或被限流
            // 进行相应的处理操作
        } finally {
            if (entry != null)
                entry.exit();
        }
    }



    private static void initFlowQpsRule() {
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule();
        rule.setResource("test");
        //
        rule.setCount(20);
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setLimitApp("default");
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
    }
}
