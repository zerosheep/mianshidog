package com.star.mianshidog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * packageName com.star.mianshidog.aop
 *
 * @author zerostart
 * @className DistributedLock
 * @date 2024/10/1
 * @description
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DistributedLock {


    String key();

    long leaseTime() default 30000;

    long waitTime() default 10000;

    TimeUnit  timeUnit() default TimeUnit.MILLISECONDS;
}
