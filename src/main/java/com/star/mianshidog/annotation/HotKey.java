package com.star.mianshidog.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * packageName com.star.mianshidog.annotation
 *
 * @author zerostart
 * @className HotKey
 * @date 2024/10/2
 * @description
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface HotKey {

    String key() default "";
}
