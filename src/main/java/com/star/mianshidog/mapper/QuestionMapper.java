package com.star.mianshidog.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.mianshidog.model.entity.Question;
import java.util.Date;
import java.util.List;


public interface QuestionMapper extends BaseMapper<Question> {

    List<Question> listQuestionWithDelete(Date fiveMinutesAgoDate);
}