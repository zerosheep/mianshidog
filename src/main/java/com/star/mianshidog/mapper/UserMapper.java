package com.star.mianshidog.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.mianshidog.model.entity.User;

/**
 * 用户数据库操作
 *
 
 */
public interface UserMapper extends BaseMapper<User> {

}




