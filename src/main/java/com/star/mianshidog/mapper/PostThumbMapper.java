package com.star.mianshidog.mapper;

import com.star.mianshidog.model.entity.PostThumb;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 帖子点赞数据库操作
 *
 
 */
public interface PostThumbMapper extends BaseMapper<PostThumb> {

}




