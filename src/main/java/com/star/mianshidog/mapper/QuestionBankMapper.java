package com.star.mianshidog.mapper;

import com.star.mianshidog.model.entity.QuestionBank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface QuestionBankMapper extends BaseMapper<QuestionBank> {

}