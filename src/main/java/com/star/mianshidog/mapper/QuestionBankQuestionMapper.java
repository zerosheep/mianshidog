package com.star.mianshidog.mapper;

import com.star.mianshidog.model.entity.QuestionBankQuestion;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


public interface QuestionBankQuestionMapper extends BaseMapper<QuestionBankQuestion> {

}