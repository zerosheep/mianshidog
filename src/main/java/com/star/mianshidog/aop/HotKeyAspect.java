package com.star.mianshidog.aop;

import com.jd.platform.hotkey.client.callback.JdHotKeyStore;
import com.star.mianshidog.annotation.AuthCheck;
import com.star.mianshidog.annotation.HotKey;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * packageName com.star.mianshidog.aop
 *
 * @author zerostart
 * @className HotKeyAspect
 * @date 2024/10/2
 * @description
 */
@Component
@Aspect
public class HotKeyAspect {


    @Around("@annotation(hotKey)")
    public Object doInterceptor(ProceedingJoinPoint joinPoint, HotKey hotKey) throws Throwable {
        String key = hotKey.key();
        if (JdHotKeyStore.isHotKey(key)) {
            Object cachedHotKey = JdHotKeyStore.get(key);
            if (cachedHotKey != null) {
                return cachedHotKey;
            }
        }
        Object result = joinPoint.proceed();
        JdHotKeyStore.smartSet(key, result);
        return result;
    }
}
