package com.star.mianshidog.constant;

/**
 * packageName com.star.mianshidog.constant
 *
 * @author zerostart
 * @className RedisConstant
 * @date 2024/9/24
 * @description
 */
public interface RedisConstant {

    /**
     * 用户签到记录的 Redis Key 前缀
     */
    String USER_SIGN_IN_REDIS_KEY_PREFIX = "user:signins";


    /**
     * 热门题库key
     */

    String BANK_HOT_KEY = "bank_detail_";

    /**
     * 获取用户签到记录的 Redis Key
     * @param year 年份
     * @param userId 用户 id
     * @return 拼接好的 Redis Key
     */
    static String getUserSignInRedisKey(int year, long userId) {
        return String.format("%s:%s:%s", USER_SIGN_IN_REDIS_KEY_PREFIX, year, userId);
    }

}
