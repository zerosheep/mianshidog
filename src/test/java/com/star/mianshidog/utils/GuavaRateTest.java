package com.star.mianshidog.utils;

import com.google.common.util.concurrent.RateLimiter;
import org.junit.jupiter.api.Test;
import org.redisson.api.RRateLimiter;
import org.redisson.api.RateIntervalUnit;
import org.redisson.api.RateType;
import org.redisson.api.RedissonClient;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * packageName com.star.mianshidog.utils
 *
 * @author zerostart
 * @className GuavaRateTest
 * @date 2024/10/3
 * @description
 */



@SpringBootTest
public class GuavaRateTest {

    RateLimiter rateLimiter = RateLimiter.create(2.0);

    @Resource
    private RedissonClient redissonClient;
    @Test
    void guavaRateLimiter() {
        for (int i = 0; i < 10; i++) {
            rateLimiter.acquire();
            System.out.println("请求"+ i + "在" + System.currentTimeMillis() + "执行");
        }
    }








    @Test
    void redissonRateLimiter() {

        RRateLimiter rateLimiter = redissonClient.getRateLimiter("myRateLimiter");
        //初始化限流器，设置速录：每一秒3个请求
        rateLimiter.trySetRate(RateType.OVERALL, 3, 1, RateIntervalUnit.SECONDS);

        for (int i = 0; i < 10; i++) {
            boolean acquired = rateLimiter.tryAcquire(1);
            System.out.println("Request " + (i + 1) + " is allowed: " + acquired);
        }


    }


}