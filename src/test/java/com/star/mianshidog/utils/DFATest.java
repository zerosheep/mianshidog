package com.star.mianshidog.utils;

import cn.hutool.dfa.WordTree;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

/**
 * packageName com.star.mianshidog.utils
 *
 * @author zerostart
 * @className DFATest
 * @date 2024/10/20
 * @description
 */

public class DFATest {


    @Test
    void test() {
        WordTree wordTree = new WordTree();
        List<String> BLACK_LIST = Arrays.asList("192.168.0.1", "192.168.0.2");
        wordTree.addWords(BLACK_LIST);
        String match = wordTree.match("192.168.2.1");

        System.out.println(match);
    }
}
