package com.star.mianshidog.utils;

import com.github.rholder.retry.*;
import com.google.common.base.Predicates;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

/**
 * packageName com.star.mianshidog.utils
 *
 * @author zerostart
 * @className RetryTest
 * @date 2024/9/28
 * @description
 */
public class RetryTest {

    @Test
    void retryTest() {
        Callable<Boolean> callable = () -> {
            System.out.println("执行重试");
            throw new IOException();
        };

        Retryer<Boolean> retryer = RetryerBuilder.<Boolean>newBuilder()
                .retryIfResult(Predicates.isNull())
                .retryIfExceptionOfType(IOException.class)
                .retryIfRuntimeException()
                .withWaitStrategy(WaitStrategies.incrementingWait(10, TimeUnit.SECONDS, 10, TimeUnit.SECONDS))
                .withStopStrategy(StopStrategies.stopAfterAttempt(3))
                .build();

        try {
            retryer.call(callable); // 执行
        } catch (RetryException | ExecutionException e) { // 重试次数超过阈值或被强制中断
            e.printStackTrace();
        }

    }

}
